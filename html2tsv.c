#include <u.h>
#include <libc.h>
#include <bio.h>

#define MAXTAGSZ 50

void
main(int argc, char *argv[])
{
	Biobuf bstdin;
	Biobuf bstdout;
	long r;
	Rune rune;
	char tagname[MAXTAGSZ];
	long tagname_i;
	
	Binit(&bstdin, 0, OREAD);
	Binit(&bstdout, 1, OWRITE);
	
	while (0 < (r = Bgetrune(&bstdin))) {
		rune = (Rune) r;
		if (rune == '<') {
			for (r = Bgetrune(&bstdin), tagname_i = 0; 0 < r && r != '>'; r = Bgetrune(&bstdin)) {
				rune = (Rune) r;
				tagname_i += runetochar(tagname + tagname_i, &rune);
			}
			tagname[tagname_i] = '\0';
			if (0 == strcmp(tagname, "/tr")) {
				Bputc(&bstdout, '\n');
			} else if (0 == strcmp(tagname, "td")) {
				Bputc(&bstdout, '"');
			} else if (0 == strcmp(tagname, "/td")) {
				Bprint(&bstdout, "\"\t");
			} else {
				/*FIXME: print formatting as plaintext?*/;
			}
		} else {
			switch (rune) {
			case '\\':
				Bprint(&bstdout, "\\\\");
				break;
			case '\t':
				Bprint(&bstdout, "\\t");
				break;
			case '\n':
				Bprint(&bstdout, "\\n");
				break;
			case '"':
				Bprint(&bstdout, "\\\"");
				break;
			default:
				Bputrune(&bstdout, rune);
			}
		}
	}
	Bflush(&bstdout);
	exit(0);
}

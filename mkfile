<$PLAN9/src/mkhdr

MKSHELL=rc

nl='
'

<semester

<|rc ./config

chunks/%.html:
	hget -p 'melyik=kodalapjan&felev=' ^ $semester ^ '&limit=1000000&targykod=' ^ $stem \
		'http://to.ttk.elte.hu/test.php' > $target
	if (! grep '^<' $target > /dev/null) {
		echo -n 'WARNING: weird chunk, replacing it with empty file' >[1=2]
		mv $target $target.weird
		touch $target
	}
	if not {
		status=''
	}


chunks/%.tsv: html2tsv chunks/%.html
	cat chunks/$stem.html | ./html2tsv | sed 's/	$//g' > $target

headers/%.tsv: chunks/%.tsv
	read chunks/$stem.tsv > $target

bodies/%.tsv: chunks/%.tsv
	cat chunks/$stem.tsv | @{read >/dev/null; cat} | sort > $target

header.tsv: $chunkheaders
	for (f in $chunkheaders) {
		if (! eval 'cmp $chunkheaders(1) $f') {
			eval 'echo ''differing chunk headers'' $f $chunkheaders(0)'
			exit 1
		}
		if not {
			status=''
		}
	}
	#get around mk variable subtitution fucking things up
	eval 'cp $chunkheaders(1) $target'

column_numbers: header.tsv mkfile
	cat header.tsv | tr '	' $nl | @{
		n=1
		while (line=`{read}) {
			echo $n^'	'^$line
			n=`{echo $n'+1' | bc | sed 1q}
		}
		status=''
	} | tee $target

body.tsv: $chunkbodies
	sort -m -o $target $chunkbodies
	uniq $target > $target.tmp
	mv $target.tmp $target

html2tsv: html2tsv.c
	9 $CC html2tsv.c
	9 $LD -o html2tsv html2tsv.$O

nuke:V:
	# doesn't nuke dataset or html2tsv
	rm chunks/* bodies/* headers/*

data/data.tsv: body.tsv header.tsv
	cat header.tsv body.tsv > $target

all: data/data.tsv

# WARNING
You probably just want [the data](https://gitlab.com/raingloom/elte-classes-data/)
this is just the code that generates it and it's probably cumbersome to set up
if you don't already know UNIX/Plan 9 programming.

# WHAT
scripts for downloading the course times of ELTE for offline work

# WHY
so that I can pick courses more easily

# BUILDING
 - install [plan9port](https://en.wikipedia.org/wiki/Plan_9_from_user_space)
   - i use [the one from Arch](https://www.archlinux.org/packages/?&q=plan9port)
 - run `mk all`
 - use `data.tsv` for whatever you want
